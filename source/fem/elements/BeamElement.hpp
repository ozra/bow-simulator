#pragma once
#include "fem/Element.hpp"
#include "fem/Node.hpp"
#include "numerics/Eigen.hpp"

class BeamElement: public Element
{
public:
    BeamElement(System& system, Node node0, Node node1, double rhoA, double L);

    void set_reference_angles(double phi_ref_0, double phi_ref_1);
    void set_stiffness(double Cee, double Ckk, double Cek);
    void set_length(double length);
    double get_epsilon() const;
    double get_kappa(double p) const;

    virtual void add_masses() const override;
    virtual void add_internal_forces() const override;
    virtual void add_tangent_stiffness() const override;

    virtual double get_potential_energy() const override;
    virtual double get_kinetic_energy() const override;

private:
    std::array<Dof, 6> dofs;

    Matrix<3, 3> C;
    double phi_ref_0;
    double phi_ref_1;
    double rhoA;
    double L;

    Vector<3> get_e() const;
    Matrix<3, 6> get_J() const;
};

