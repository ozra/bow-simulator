Bow and arrow physics simulation. Visit https://bow-simulator.sourceforge.io/ for more information.


# Build on Windows

## Prerequisites

* Visual Studio Community 2015
* Qt 5.9.1 (msvc2015 32bit)
* Python
* Cmake
* rcedit (https://github.com/electron/rcedit/releases)
* Inno Setup (http://www.jrsoftware.org/isinfo.php)

## Building

Execute build.py in the VS 2015 Developer Command Prompt. This script downloads the dependencies, compiles the application and creates a Windows installer package.
